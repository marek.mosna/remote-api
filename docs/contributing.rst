Contributing
============

.. toctree::
   :maxdepth: 2

This guide assumes you have forked and checked-out the repository.
It is recommended that you install sl1fw-api in a virtual environment
like conda or virtualenv.

Run dev
-------
Run development mode.

.. code-block:: shell

    $ ./scripts/dev.sh

pre-commit
----------
For static analysis and styler checker, we use ``pre-commit``.

**run pre-commit install to set up the git hook scripts:**

.. code-block:: shell

    $ pre-commit install

Now pre-commit will analyze code before commit automatically.

**If you want to see all static analysis warnings run:**

.. code-block:: shell

    $ pre-commit run --all-files


Create documentation
--------------------
.. code-block:: shell

    $ cd docs
    $ make html

