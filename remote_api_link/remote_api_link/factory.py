# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from time import sleep
from pathlib import Path
from logging.config import dictConfig

from flask import Flask, current_app, request
from werkzeug.routing import Rule
from werkzeug.exceptions import NotFound, MethodNotAllowed
from gi.repository.GLib import GError

from remote_api_link import defines
from remote_api_link.printer import Printer
from remote_api_link.exceptions import (
    Unknown,
    ApiException,
    Unauthorized,
    RemoteApiError,
    generate_error_page,
)


def configure_log() -> None:
    """Configuring the logs"""

    level = logging.INFO
    if Path(defines.DEBUG_FILE).exists():
        level = logging.DEBUG
    dictConfig(
        {
            "version": 1,
            "formatters": {
                "remote_api_link": {"format": "%(levelname)s - %(name)s - %(message)s"}
            },
            "handlers": {
                "journald": {
                    "class": "systemd.journal.JournalHandler",
                    "formatter": "remote_api_link",
                    "SYSLOG_IDENTIFIER": "REMOTE_API_LINK",
                }
            },
            "root": {"level": logging.getLevelName(level), "handlers": ["journald"]},
        }
    )


def before_request():
    """Check if the user is authorized

    Raises:
        ApiKeyMissing: ApiKey is not set

    """
    if request.path not in ["/error", "/error_401"]:
        current_app.printer.validate_auth(request.headers.get("X-Api-Key", None))


def error_401():
    """respond the unauthorized error

    Returns:
        (string, int): respond (text/json), status code (401)
    """
    return Unauthorized().get_response()


def create_rule_all_method(app, rule, endpoint, func):
    app.view_functions[endpoint] = func
    app.url_map.add(Rule(rule, endpoint=endpoint))


def handle_exception(e):
    printer = current_app.printer
    logger = current_app.logger
    logger.exception(e)
    exception = e

    if isinstance(e, NotFound) or isinstance(e, MethodNotAllowed):
        logger.error("%s %s %s", request.method, request.path, e.__class__.__name__)
        exception = RemoteApiError()
    elif not isinstance(e, ApiException):
        logger.error("Error when %s %s", request.method, request.path)
        exception = Unknown()

    if exception.code != 401:  # Unauthorized
        exception.log()
        printer.last_error_or_warn = exception
    return exception.get_response()


def create_app():  # pragma: no cover
    configure_log()
    logger = logging.getLogger()
    printer = None
    timeout_s = 30
    logger.info("connecting with the printer dbus")
    for i in range(timeout_s):
        try:
            printer = Printer()
            break
        except GError as err:
            if i + 1 == timeout_s:
                raise err
            logger.warning(err.message)
            sleep(1)

    if printer is None:
        logger.error("fail to connect with the printer dbus")
        return None

    app = Flask(__name__)
    app.config.from_pyfile("config.cfg")

    app.printer = printer

    from remote_api_link.api import api

    app.register_blueprint(api, url_prefix="/api")

    app.before_request(before_request)
    app.register_error_handler(Exception, handle_exception)
    create_rule_all_method(app, "/error", "error_page", generate_error_page)
    create_rule_all_method(app, "/error_401", "error_401", error_401)
    return app
