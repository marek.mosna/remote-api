# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# flake8: noqa

from .base import ApiException, generate_error_page
from .exception import (
    Unknown,
    ApiKeyMissing,
    FileNotFound,
    FileAlreadyExists,
    NotMechanicallyCalibrated,
    NotUvCalibrated,
    InvalidProject,
    NotAvailableInState,
    NotEnoughInternalSpace,
    RemoteApiError,
    Unauthorized,
    DirectoryNotEmpty,
    ProjectErrorCantRemove,
    get_error_by_code,
    ProjectErrorCorrupted,
    ProjectErrorCantRead,
    InvalidExtension,
)
