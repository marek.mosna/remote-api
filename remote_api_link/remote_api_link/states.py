# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from enum import unique, Enum


@unique
class State(Enum):
    """
    General printer state enumeration
    """

    READY = 0
    SELECTED = 1
    PRINTING = 2
    POUR_IN_RESIN = 3
    BUSY = 5
    FEED_ME = 6
    ERROR = 9

    def get_flags(self):
        flags = {
            "operational": True,
            "paused": False,
            "printing": False,
            "cancelling": False,
            "pausing": False,
            "sdReady": True,
            "error": False,
            "ready": False,
            "closedOrError": False,
        }

        if self == self.READY or self == self.SELECTED:
            flags["ready"] = True
        elif self == self.PRINTING:
            text = "Printing"
            flags["printing"] = True
        elif self == self.POUR_IN_RESIN or self == self.FEED_ME:
            flags["printing"] = True
            flags["paused"] = True
            if self == self.POUR_IN_RESIN:
                text = "Pour in resin"
            else:
                text = "Feed me"
        elif self == self.BUSY:
            flags["operational"] = False
            flags["ready"] = True
        elif self == self.ERROR:
            text = "ClosedOrError"
            flags["operational"] = False
            flags["closedOrError"] = True

        text = self.name.capitalize().replace("_", " ")

        return {
            "text": text,
            "flags": flags,
        }
