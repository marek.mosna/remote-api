#!/usr/bin/env python

# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from threading import Thread

from gevent.pywsgi import WSGIServer
from gi.repository import GLib

from remote_api_link.factory import create_app


if __name__ == "__main__":
    app = create_app()
    if app is None:
        exit(1)

    Thread(target=GLib.MainLoop().run).start()
    http_server = WSGIServer(("", 8080), application=app, log=None)
    http_server.serve_forever()
