# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import List

from flask import current_app, request, jsonify

from . import api
from ..states import State


@api.route("/printer", methods=["GET"])
def printer_telemetry():
    excludes: List[str] = []
    exclude_attr = request.values.get("exclude")
    if exclude_attr:
        excludes = [
            e
            for e in exclude_attr.strip().split(",")
            if e in ["temperature", "sd", "state", "telemetry"]
        ]

    telemetry = current_app.printer.sys.hw_telemetry
    temperatures = telemetry["temperatures"]
    result = {}
    if "temperature" not in excludes:
        result["temperature"] = {
            "tool0": {"actual": temperatures["temp_led"], "target": 0, "offset": 0},
            "bed": {"actual": temperatures["cpu_temp"], "target": 0, "offset": 0},
            "chamber": {"actual": temperatures["temp_amb"], "target": 0, "offset": 0},
        }

    if "state" not in excludes:
        result["state"] = State(telemetry["state"]).get_flags()

    if "telemetry" not in excludes:
        fans = telemetry["fans"]
        result["telemetry"] = {
            "tempCpu": temperatures["cpu_temp"],
            "tempUvLed": temperatures["temp_led"],
            "tempAmbient": temperatures["temp_amb"],
            "fanUvLed": fans["uv_led"],
            "fanBlower": fans["blower"],
            "fanRear": fans["rear"],
            "coverClosed": telemetry["cover_closed"],
        }

    if "sd" not in excludes:
        result["sd"] = (
            {
                "ready": False,
            },
        )

    return result


@api.route("/printer/sd", methods=["GET"])
def printer_sd_state():
    return jsonify({"ready": False})


@api.route("/printer/error", methods=["GET"])
def printer_error():
    exception = current_app.printer.last_error_or_warn
    if exception:
        response = exception.get_response()
        response.status_code = 200
        return response

    accept = request.headers.get("Accept")
    if accept and "json" in accept:
        return jsonify({})
    return _("NO ERROR")
