# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import time
import threading
import tempfile
import requests

from typing import Dict
from requests.models import Response
from requests.exceptions import RequestException
from flask import current_app, send_file, request
from flask.json import jsonify

from . import api
from remote_api_link.exceptions import (
    RemoteApiError,
    FileAlreadyExists,
    ProjectErrorCantRead,
)
from remote_api_link.api.files import validate_project

from prusaerrors.sl1.codes import Sl1Codes


class DownloadMgr:
    def __init__(self, printer):
        self.response = {}
        self.thread = threading.Thread()
        self._upload_handle = -1
        self._update_timer: threading.Timer = None
        self._printer = printer

    def start(self, target: str, data: Dict):
        if self._printer.download_mgr.thread.is_alive():
            raise FileAlreadyExists("Uploading in progress")
        self._upload_handle = -1
        try:
            req = requests.get(data["url"], stream=True, allow_redirects=True)
            filename = data["url"].split("/")[-1]
            content_length = int(req.headers["Content-Length"])
            file_path, self._upload_handle = validate_project(
                filename, target, data["destination"], content_length
            )
            self._printer.download_mgr.thread = threading.Thread(
                target=self._download, args=(req, target, file_path), daemon=True
            )
            self.thread.start()
        except RequestException:
            self._printer.upload_error(
                self._upload_handle, Sl1Codes.PROJECT_ERROR_CANT_READ
            )
            raise ProjectErrorCantRead()

    def _download(self, req: Response, target: str, path: str):
        total_size = int(req.headers.get("content-length"))
        actual_size = 0
        self.response = {
            "url": req.url,
            "target": target,
            "destination": path,
            "size": total_size,
            "start_time": time.time(),
            "progress": 0,
            "remaining_time": 0,
            "to_select": False,
            "to_print": False,
        }
        with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
            for chunk in req.iter_content(chunk_size=2048):
                if chunk:
                    tmp_file.write(chunk)
                    actual_size += len(chunk)
                    self.response["progress"] = actual_size / total_size

                    # limit dbus update notification events
                    if self._update_timer is None or not self._update_timer.is_alive():
                        self._update_timer = threading.Timer(0.5, self._update_progress)
                        self._update_timer.start()

        self._printer.fs.move(tmp_file.name, self.response["destination"])
        self._printer.upload_modify(self._upload_handle, "progress", 1)

    def _update_progress(self):
        self._printer.upload_modify(
            self._upload_handle, "progress", self.response["progress"]
        )


@api.route("/downloads/<string:target>/<path:filename>", methods=["GET"])
def downloads(target, filename):
    printer = current_app.printer
    path = printer.convert_path(target, filename)  # convert path
    printer.fs.get_from_path(path, 0)  # check if exist
    return send_file(
        path,
        attachment_filename=os.path.basename(path),
    )


@api.route("/download", methods=["GET"])
def get_download_file_progress():
    mgr = current_app.printer.download_mgr
    if mgr.thread.is_alive():
        return jsonify(current_app.printer.download_mgr.response), 200
    else:
        return "", 204


@api.route("/download/<string:target>", methods=["POST"])
def download_file_remote_url(target):
    data = request.json
    if None in (data["url"], data["destination"]):
        return RemoteApiError()
    current_app.printer.download_mgr.start(target, data)
    return "", 201
