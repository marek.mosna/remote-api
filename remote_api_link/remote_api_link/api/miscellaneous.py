# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import current_app, jsonify, request, url_for

from . import api

from remote_api_link import defines
from remote_api_link.states import State


@api.route("/version", methods=["GET"])
def version():
    return jsonify(
        {
            "api": "0.1",
            "server": "1.1.0",
            "text": "Prusa SLA 1.0.5",
            "hostname": current_app.printer.sys.net_hostname,
        }
    )


@api.route("/connection", methods=["GET", "POST"])
def connection():
    if request.method == "POST":
        return ("", 204)
    else:
        api_state = State(current_app.printer.sys.state).get_flags()
        return jsonify(
            {
                "current": {
                    "baudrate": 115200,
                    "port": "VIRTUAL",
                    "printerProfile": "_default",
                    "state": api_state["text"],
                },
                "options": {
                    "baudratePreference": 115200,
                    "baudrates": [115200],
                    "portPreference": "VIRTUAL",
                    "ports": ["VIRTUAL"],
                    "printerProfilePreference": "_default",
                    "printerProfiles": [
                        {"id": "_default", "name": "Default"},
                    ],
                },
            }
        )


def get_defalt_profile():
    project_extensions = current_app.printer.project_extensions
    return {
        "id": "_default",
        "name": "Default",
        "model": "Original Prusa SLA",
        "color": "default",
        "current": True,
        "default": True,
        "resource": url_for(".get_printer_profile", _external=True),
        "heatedBed": True,
        "heatedChamber": True,
        "projectExtensions": project_extensions,
        "extruder": {
            "count": 1,
            "offsets": [0.0, 0.0],
        },
    }


@api.route("/printerprofiles", methods=["GET"])
def printer_profiles():
    return jsonify(
        {
            "profiles": [get_defalt_profile()],
        }
    )


@api.route("/printerprofiles/_default", methods=["GET"])
def get_printer_profile():
    return jsonify(get_defalt_profile())


@api.route("/access/users", methods=["GET"])
def access_users():
    return jsonify(
        {
            "users": [
                {
                    "active": True,
                    "apikey": None,
                    "name": "maker",
                    "settings": {},
                    "user": True,
                    "admin": False,
                },
            ],
        }
    )


@api.route("/login", methods=["GET", "POST"])
def login():
    return {
        "session": "dummy",
        "_is_external_client": True,
    }


@api.route("/logout", methods=["POST"])
def logout():
    return defines.NO_CONTENT


@api.route("/currentuser", methods=["GET"])
def get_current_user():
    return {
        "name": "Prusa",
    }


@api.route("/timelapse", methods=["GET", "POST"])
def timelapse():
    return jsonify({"config": {"type": "off"}, "enabled": False, "files": []})


@api.route("/settings", methods=["GET", "POST"])
def settings():
    return {"appearance": {"color": "black"}}
