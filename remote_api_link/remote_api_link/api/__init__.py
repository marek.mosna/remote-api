# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import Blueprint

api = Blueprint("api", __name__)

from . import download  # noqa: F401, E402
from . import files  # noqa: F401, E402
from . import job  # noqa: F401, E402
from . import miscellaneous  # noqa: F401, E402
from . import printer  # noqa: F401, E402
from . import system  # noqa: F401, E402
from . import thumbnails  # noqa: F401, E402
