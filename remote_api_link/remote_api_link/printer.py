# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=no-member

import os
import re
import json
import gettext
import logging
from datetime import datetime, timezone

from flask import current_app
from pydbus import SystemBus
from gi.repository.GLib import Variant
from gevent.lock import BoundedSemaphore

from remote_api_link.dbus import create_bus
from remote_api_link import defines
from remote_api_link.api.download import DownloadMgr
from remote_api_link.exceptions import ApiKeyMissing, FileNotFound, get_error_by_code


class Printer:
    def __init__(self):
        self._logger = logging.getLogger()
        self._last_error_or_warn_lock = BoundedSemaphore()
        self._last_error_or_warn = None

        self._logger.info("Registering event handlers")
        bus = SystemBus()
        lang = bus.get("org.freedesktop.locale1")
        lang.PropertiesChanged.connect(self._locale_changed)
        self._language = None
        self._locale_changed(None, {"Locale": lang.Locale}, None)

        self._logger.info("Connecting dbus interface")
        self._fs = create_bus(bus, "cz.prusa3d.sl1.filemanager0")
        self._download_mgr = DownloadMgr(self)
        self._fs.bus_obj.PropertiesChanged.connect(self._on_filemanager_changed)
        self._project_extensions = self._fs.bus_obj.extensions
        self._last_update_lock = BoundedSemaphore()
        self._last_modified = None
        self._roots = {}
        self._protected_roots = {}
        self._on_filemanager_changed(
            None, {"last_update": self._fs.bus_obj.last_update}, None
        )

        self._sys = create_bus(bus, "cz.prusa3d.sl1.standard0")
        self._sys.bus_obj.PropertiesChanged.connect(self._on_standard_changed)
        self._sys.bus_obj.LastErrorOrWarn.connect(self._on_last_error_or_warn_changed)
        self._authorization = self._sys.net_authorization
        self._auth_lock = BoundedSemaphore()

        self._notify = bus.get("cz.prusa3d.sl1.Notify1")

    @property
    def fs(self):
        return self._fs

    @property
    def download_mgr(self):
        return self._download_mgr

    @property
    def sys(self):
        return self._sys

    @property
    def project_extensions(self):
        return self._project_extensions

    def _locale_changed(self, __, changed, ___):
        if "Locale" not in changed:
            return

        lang = re.sub(r"LANG=(.*)\..*", r"\g<1>", changed["Locale"][0])

        try:
            if lang not in [
                "cs_CZ",
                "de_DE",
                "en_US",
                "es_ES",
                "fr_FR",
                "it_IT",
                "pl_PL",
            ]:
                lang = "en_US"
            translation = gettext.translation(
                "remote_api_link",
                localedir=defines.localedir,
                languages=[lang],
                fallback=True,
            )
            translation.install(names="ngettext")
            self._language = lang
        except (IOError, OSError):
            self._logger.exception("Translation for %s cannot be installed.", lang)

    @property
    def locale(self):
        return self._language[0:2]

    def _on_standard_changed(self, __, changed, ___):
        if "net_authorization" not in changed:
            return

        if self._auth_lock.acquire():
            self._authorization = changed["net_authorization"]
            self._auth_lock.release()

    def _on_filemanager_changed(self, __, changed, ___):
        if "last_update" not in changed:
            return

        if self._last_update_lock.acquire():
            self._last_modified = datetime.now(timezone.utc).strftime(
                defines.HEADER_TIME_FORMAT
            )

            self._roots.clear()
            for k, v in self._fs.get_all(0).items():
                if k not in ["local", "usb"]:
                    if k != "last_update":
                        self._protected_roots[k] = v["path"]
                    continue
                self._roots[k] = v["path"]

            self._last_update_lock.release()

    def validate_auth(self, apikey_client) -> None:
        try:
            if self._auth_lock.acquire():

                # digest by nginx
                if self._authorization["type"] == "digest":
                    return

                # apikey by flask
                if self._authorization["api_key"] != apikey_client:
                    raise ApiKeyMissing()
        finally:
            self._auth_lock.release()

    def is_not_modified(self, headers) -> bool:
        if (etag := headers.get("If-None-Match", None)) is not None:
            return etag != "null" and etag == self._fs.etag["etag"]
        if (last_modified := headers.get("If-Modified-Since")) is not None:
            return last_modified != "null" and last_modified == self.last_modified
        return False

    @property
    def last_modified(self) -> str:
        try:
            if self._last_update_lock.acquire():
                return self._last_modified
            return None
        finally:
            self._last_update_lock.release()

    def convert_path(self, target, filename="", permite_other_sources=[]) -> str:
        try:
            if self._last_update_lock.acquire():
                if target in self._roots:
                    return os.path.join(self._roots[target], filename)
                if target in permite_other_sources and target in self._protected_roots:
                    return os.path.join(self._protected_roots[target], filename)
            raise FileNotFound()
        finally:
            self._last_update_lock.release()

    def make_response(self, data):
        return current_app.response_class(
            response=json.dumps(data) + "\n",
            status=200,
            mimetype="application/json",
            headers={
                "ETag": self._fs.etag["etag"],
                "Last-Modified": self.last_modified,
                "Date": datetime.utcnow().strftime(defines.HEADER_TIME_FORMAT),
            },
        )

    def upload_number(self, filename: str, progress: int, size: int, path: str) -> int:
        return self._notify.addFileUploadNotification(filename, progress, size, path)

    def upload_modify(self, handle: int, name: str, value: int) -> bool:
        return self._notify.modify(handle, name, Variant("d", value))

    def upload_error(self, handle: int, error) -> bool:
        return self._notify.modify(handle, "errorCode", Variant("s", error.code))

    def upload_remove(self, handle: int) -> bool:
        return self._notify.removeNotification(handle)

    @property
    def last_error_or_warn(self):
        try:
            if self._last_error_or_warn_lock.acquire():
                return self._last_error_or_warn
        finally:
            self._last_error_or_warn_lock.release()

    @last_error_or_warn.setter
    def last_error_or_warn(self, value):
        try:
            if self._last_error_or_warn_lock.acquire():
                self._last_error_or_warn = value
        finally:
            self._last_error_or_warn_lock.release()

    def _on_last_error_or_warn_changed(self, error):
        self.last_error_or_warn = get_error_by_code(error)
