# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Any


def valid_boolean_trues(value: Any) -> bool:
    if isinstance(value, str):
        return value.lower() in ("true", "yes", "y", "1")
    else:
        return value in (True, 1)
