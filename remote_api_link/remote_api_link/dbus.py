# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import xml.etree.ElementTree as ET  # nosec

from gi.repository.GLib import GError

from remote_api_link.exceptions import get_error_by_code


def create_method(name):
    def wrapper(self, *args, **kwds):
        try:
            return getattr(self.bus_obj, name)(*args, **kwds)
        except GError as error:
            last_exception = self.bus_obj.last_exception
            self._logger.error(
                "the error code: %s text: %s happens when call [%s].%s",
                last_exception["code"],
                last_exception.get("text", ""),
                self.__class__.__name__,
                name,
            )
            exception = get_error_by_code(last_exception)
            if exception:
                raise exception from error

    wrapper.__name__ = name
    return wrapper


def create_property(name):
    def get_wrapper(self, *args, **kwds):
        try:
            return getattr(self.bus_obj, name)
        except GError as error:
            last_exception = self.bus_obj.last_exception
            self._logger.error(
                "the error code: %s text: %s happens when get [%s].%s",
                last_exception["code"],
                last_exception.get("text", ""),
                self.__class__.__name__,
                name,
            )
            exception = get_error_by_code(last_exception)
            if exception:
                raise exception from error

    def set_wrapper(self, value):
        try:
            return setattr(self.bus_obj, name, value)
        except GError as error:
            last_exception = self.bus_obj.last_exception
            self._logger.error(
                "the error code: %s text: %s happens when set [%s].%s",
                last_exception["code"],
                last_exception.get("text", ""),
                self.__class__.__name__,
                name,
            )
            exception = get_error_by_code(last_exception)
            if exception:
                raise exception from error

    return property(get_wrapper, set_wrapper)


def constructor(self, bus_obj):
    self.bus_obj = bus_obj
    self._logger = logging.getLogger()


def create_bus(bus, interface_name):
    bus_obj = bus.get(interface_name)
    root = ET.fromstring(bus_obj.Introspect())
    interface = root.findall(f"./interface[@name='{interface_name}']/*")

    members = {}
    for child in interface:
        func_name = child.attrib["name"]
        if child.tag == "property":
            members[func_name] = create_property(func_name)
        else:
            members[func_name] = create_method(func_name)

    members["__init__"] = constructor
    return type(interface_name, (object,), members)(bus_obj)
