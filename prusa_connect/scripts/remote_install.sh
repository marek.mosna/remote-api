#!/bin/sh

# This file is part of the SL1 firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# Resolve target
if [ "$#" -ne 1 ]; then
    echo "Please provide target ip as the only argument"
    exit -1
fi
target=${1}
echo "Target is ${target}"

# Print commands being executed
set -o xtrace

# Create temp root
tmp=$(mktemp --directory --tmpdir=/tmp/ prusa_connect.XXXX)
echo "Local temp is ${tmp}"

echo "Running setup"
export PYTHONPATH="$(pwd)$(find ../dependencies/ -maxdepth 1 -type d -printf ':%p')"
python3 setup.py sdist --dist-dir=${tmp}

# Create remote temp
target_tmp=$(ssh root@${target} "mktemp --directory --tmpdir=/tmp/ prusa_connect.XXXX")
echo "Remote temp is ${target_tmp}"

echo "Installing on target"
scp -r ${tmp}/* root@${target}:${target_tmp}
ssh root@${target} "\
set -o xtrace; \
cd ${target_tmp}; \
tar xvf prusa_connect*.tar.gz; \
rm prusa_connect*.tar.gz; \
cd prusa_connect-*; \
rm -fr /usr/lib/python*/site-packages/prusa/connect/sl1
rm -fr /usr/lib/python*/site-packages/prusa_connect-*.egg-info
rm -f /usr/lib/systemd/system/prusa_connect.service
rm -f /usr/share/dbus-1/system.d/cz.prusa3d.connect0.conf
pip3 install . ; \
systemctl daemon-reload; \
systemctl restart prusa_connect
"

echo "Removing remote temp"
ssh root@${target} "rm -rf ${target_tmp}"

echo "Removing local temp"
rm -rf ${tmp}
rm -rf prusa_connect.egg-info
