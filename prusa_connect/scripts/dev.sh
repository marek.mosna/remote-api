#!/bin/sh

# This file is part of the SL1 firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

PYTHON="$($SHELL which python)"
export PYTHONPATH="$(pwd)$(find ../dependencies/ -maxdepth 1 -type d -printf ':%p')"
$PYTHON -m prusa.connect.sl1
