from enum import unique, Enum


@unique
class State(Enum):
    """
    General printer state enumeration
    """

    READY = 0
    SELECTED = 1
    PRINTING = 2
    ATTENTION = 3
    PRINTING_ATTENTION = 4
    PRINTING_BUSY = 5
    REFILL = 6
    BUSY = 7
    FINISHED = 8
    ERROR = 9

    @classmethod
    def get(cls, value):
        state = cls[value]
        if state in [cls.SELECTED, cls.PRINTING_ATTENTION, cls.REFILL]:
            return State.ATTENTION.name
        elif state == cls.PRINTING_BUSY:
            return cls.BUSY.name
        return state.name
