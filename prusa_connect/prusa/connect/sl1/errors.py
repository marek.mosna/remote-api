from prusaerrors.sl1.codes import Sl1Codes
from slafw.errors.exceptions import with_code
from slafw.errors.errors import GeneralError


@with_code(Sl1Codes.CONNECTION_FAILED)
class ConnectionFailedError(GeneralError):
    """CONNECTION FAILED"""

    ...
