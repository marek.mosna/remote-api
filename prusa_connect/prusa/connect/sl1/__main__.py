# This file is part of the SL1 firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from logging.config import dictConfig

from .printer import Connect


def init_logging():
    DEFAULT_CONFIG = {
        "version": 1,
        "formatters": {"slafw": {"format": "%(levelname)s - %(name)s - %(message)s"}},
        "handlers": {
            "journald": {
                "class": "systemd.journal.JournalHandler",
                "formatter": "slafw",
                "SYSLOG_IDENTIFIER": "PRUSA_CONNECT",
            }
        },
        "root": {"level": "INFO", "handlers": ["journald"]},
    }
    dictConfig(DEFAULT_CONFIG)


if __name__ == "__main__":
    init_logging()
    Connect().run()
