# -*- coding: utf-8 -*-
# This file is part of the SL1 firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# pylint: disable=C0301

from __future__ import annotations
from typing import Optional, Dict, Any, TYPE_CHECKING

from pydbus.generic import signal

from slafw.api.decorators import (
    auto_dbus,
    dbus_api,
    last_error,
    wrap_dict_data,
    wrap_exception,
)

if TYPE_CHECKING:
    from .printer import Connect


@dbus_api
class PrusaConnect0:
    """Dbus api for Prusa Connect"""

    __INTERFACE__ = "cz.prusa3d.connect0"

    PropertiesChanged = signal()

    def __init__(self, connect: "Connect"):
        self._last_exception_data: Optional[Exception] = None
        self._connect = connect

    @auto_dbus
    @property
    def last_exception(self) -> Dict[str, Any]:
        """Last Exception data

        Returns:
            Dict[str, Any]
        """
        return wrap_dict_data(wrap_exception(self._last_exception))

    @property
    def _last_exception(self) -> Exception:
        return self._last_exception_data

    @_last_exception.setter
    def _last_exception(self, value: Exception):
        self._last_exception_data = value
        self.PropertiesChanged(
            self.__INTERFACE__, {"last_exception": self.last_exception}, []
        )

    @auto_dbus
    @last_error
    def register_printer(self) -> str:
        return self._connect.register()

    @auto_dbus
    @last_error
    def confirm_registration(self, temp_token: str) -> bool:
        """Confirm the register.

        Args:
            temp_token (str): Token get in net_prusa_connect_register()

        Raises:
            Exception: when there isn't a instance of Prusa connect
                       It has to call net_prusa_connect_register() first.

        Returns:
            bool: confirm if the register was finished
        """
        if self._connect.register_confirm(temp_token):
            self.PropertiesChanged(
                self.__INTERFACE__, {"is_registered": self.is_registered}, []
            )
            return True
        return False

    @auto_dbus
    @last_error
    def reset_connect(self) -> bool:
        """Reset Prusa Connect token (Printer-token)"""
        if self._connect.reset():
            self.PropertiesChanged(
                self.__INTERFACE__, {"is_registered": self.is_registered}, []
            )
            return True
        return False

    @auto_dbus
    @property
    def is_registered(self) -> bool:
        """Return bool"""
        return bool(self._connect._prop_printer_token)
