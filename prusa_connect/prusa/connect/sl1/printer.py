# This file is part of the SL1 firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import signal
import logging
from time import sleep
import threading
from typing import Any, Optional, List, Dict

from gi.repository import GLib
from pydbus import SystemBus
from requests.exceptions import ConnectionError as SDKConnectionError
from urllib3.exceptions import HTTPError

from prusa.connect.printer import Printer as PrusaConnect, Telemetry, const, Event
from prusa.connect.printer.connection import Connection
from slafw.libConfig import TomlConfig
from slafw import defines

from .dbus import PrusaConnect0
from .errors import ConnectionFailedError
from .states import State


def flatten(d: Dict) -> Dict[str, Any]:
    new_items = {}
    for key, value in d.items():
        if isinstance(value, dict):
            new_items.update(value)
        else:
            new_items[key] = value
    return new_items


class Connect:

    SERVER = "http://dev.connect.prusa:8080"

    def __init__(self):
        self._logger = logging.getLogger(__name__)
        self._logger.info("Initializing Prusa Connect")
        self._stop_request = threading.Event()

        timeout_s = 30
        for count in range(timeout_s):
            try:
                self._printer = SystemBus().get("cz.prusa3d.sl1.standard0")
                break
            except Exception:
                if count == timeout_s - 1:
                    raise
                sleep(1)

        info = self._printer.info

        connection = Connection(self.SERVER, info["uuid"])
        self._prusa_connect = PrusaConnect(const.Printer.SL1, info["sn"], connection)
        self._prusa_connect.ip = self._printer.net_ip
        self._prusa_connect.mac = info["mac"]
        self._prusa_connect.firmware = info["firmware"]

        # registration handlers
        for cmd in const.Command:
            cmd_name = f"cmd_{cmd.value.lower()}"
            if hasattr(self, cmd_name):
                self._prusa_connect.set_handler(cmd, getattr(self, cmd_name))

        # subscribe events
        self._properties_changed_registration = self._printer.PropertiesChanged.connect(
            self._properties_changed
        )

    def signalHandler(self, _signal, _frame):
        self._logger.debug("signal received")
        self._properties_changed_registration.unsubscribe()
        self._stop_request.set()

    def send_telemetry(self):
        while not self._stop_request.is_set():
            try:
                token = self._prop_printer_token
                if token:
                    self._prusa_connect.conn.token = token
                    while not self._stop_request.is_set():
                        telemetry = flatten(self._printer.hw_telemetry)
                        state = const.State(State.get(telemetry["state"]))
                        del telemetry["state"]
                        if state == const.State.PRINTING:
                            telemetry.update(self._printer.job)
                        res = self._prusa_connect.telemetry(
                            Telemetry(state, **telemetry)
                        )
                        if res.status_code > 299:
                            self._logger.error(res.json())
                        sleep(1)
            except SDKConnectionError as error:
                self._logger.error(
                    "Failed to establish a new connection [errno %d]", error.errno
                )
            except HTTPError as error:
                if hasattr(error, "message"):
                    self._logger.warning(getattr(error, "message"))
                sleep(5)
            finally:
                sleep(5)

    def _properties_changed(self, __, changed, ___):
        if self._prop_printer_token and "state" in changed:
            self._prusa_connect.event(
                Event(
                    const.Event.STATE_CHANGED,
                    const.Source.FIRMWARE,
                    state=State.get(changed["state"]),
                )
            )

    def cmd_start_print(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"start_print: {str(args)}")

    def cmd_stop_print(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"stop_print: {str(args)}")

    def cmd_pause_print(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"pause_print: {str(args)}")

    def cmd_resume_print(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"resume_print: {str(args)}")

    def cmd_send_file_info(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"resume_print: {str(args)}")

    def cmd_delete_file(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"delete_file: {str(args)}")

    def cmd_download_file(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"download_file: {str(args)}")

    def cmd_create_directory(self, prn: PrusaConnect, args: Optional[List[Any]]) -> Any:
        print(f"create_directory: {str(args)}")

    @property
    def _prop_printer_token(self) -> Optional[str]:
        return TomlConfig(defines.remoteConfig).load().get("prusa_connect_token")

    @_prop_printer_token.setter
    def _prop_printer_token(self, token: str) -> None:
        """Set Prusa Connect token (Printer-token)

        Args:
            token (str)
        """
        remoteConfig = TomlConfig(defines.remoteConfig)
        newData = remoteConfig.load()
        newData["prusa_connect_token"] = token
        if remoteConfig.save(data=newData):
            self._logger.info("The printer token was changed.")
        else:
            self._logger.info("The printer token cannot be saved.")

    def register(self) -> str:
        try:
            return self._prusa_connect.register()
        except SDKConnectionError:
            raise ConnectionFailedError()

    def register_confirm(self, temp_token: str) -> bool:
        """Confirm the register.

        Args:
            temp_token (str): Token get in net_prusa_connect_register()

        Raises:
            Exception: when there isn't a instance of Prusa connect
                       It has to call net_prusa_connect_register() first.

        Returns:
            bool: confirm if the register was finished
        """
        try:
            printer_token = self._prusa_connect.get_token(temp_token)
            if printer_token:
                self._prop_printer_token = printer_token
                self._logger.info("Printer registred.")
                return True
        except SDKConnectionError:
            raise ConnectionFailedError()

        return False

    def reset(self) -> bool:
        """Reset Prusa Connect token (Printer-token)"""
        msg = "Reset printer token: "
        remoteConfig = TomlConfig(defines.remoteConfig)
        newData = remoteConfig.load()
        if "prusa_connect_token" in newData:
            del newData["prusa_connect_token"]
            if remoteConfig.save(data=newData):
                self._logger.info(msg + "reseted")
                return True
            else:
                self._logger.info(msg + "not saved")
        self._logger.info(msg + "no key found")
        return False

    def run(self):
        self._logger.info("Starting Prusa Connect")
        signal.signal(signal.SIGINT, self.signalHandler)
        signal.signal(signal.SIGTERM, self.signalHandler)

        loop = GLib.MainLoop()
        threading.Thread(target=loop.run, daemon=True).start()
        SystemBus().publish(PrusaConnect0.__INTERFACE__, PrusaConnect0(self))

        self.send_telemetry()
        loop.quit()
