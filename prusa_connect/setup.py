# This file is part of the SL1 firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

data_files = [
    ("/usr/lib/systemd/system", ["systemd/prusa_connect.service"]),
    ("/usr/share/dbus-1/system.d", ["systemd/cz.prusa3d.connect0.conf"]),
]

setuptools.setup(
    name="prusa_connect",
    version="0.1.0",
    description="Prusa Connect Client",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/prusa3d/sl1/remote-api/tree/master/prusa_connect",
    license="GNU General Public License v3 or later (GPLv3+)",
    data_files=data_files,
    packages=setuptools.find_namespace_packages(include=["prusa.connect.*"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires=">=3.6",
)
